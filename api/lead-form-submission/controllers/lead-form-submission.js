'use strict';

/**
 * Read the documentation (https://strapi.io/documentation/developer-docs/latest/development/backend-customization.html#core-controllers)
 * to customize this controller
 */

const {sanitizeEntity} = require('strapi-utils');


module.exports = {

  async create(ctx) {

    let entity;
    if (ctx.is('application/json')) {
      const datas = ctx.request.body;
      entity = await strapi.services['lead-form-submission'].create(datas);

      const emailTemplate = {
        subject: 'Message du site Ask for dev',
        html: `<p>Vous avez un nouveau message de <%= datas.name %></p>
            <p>Message :</p>
            <p><%= datas.message %></p>
            <p><a href="mailto:<%= datas.email %>">Répondre a cette adreese <%= datas.email %></a></p>`,
        text: `Vous avez un nouveau message de <%= datas.name %> - <%= datas.email %>
            Message :
            <%= datas.message %>`
      }

      await strapi.plugins.email.services.email.sendTemplatedEmail(
        {to: 'sebastien@askfordev.fr'},
        emailTemplate,
        {
          datas: datas,
        }
      );

      return sanitizeEntity(entity, {model: strapi.models['lead-form-submission']});
    }

  },
};
